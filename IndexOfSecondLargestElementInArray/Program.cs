﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        if (x.Length < 2) return -1;
        else
        {
            int max = FindMax(x);
            int[] temp = new int[x.Length - 1];
            int i = 0;
            int j = 0;
            do
            {
                if (x[i] != max)
                    temp[j++] = x[i++];
                else i++;
            } while (i < x.Length);
            int secondMax = FindMax(temp);
            return Array.IndexOf(x, secondMax);
        }
    }
    public static int FindMax(int[] arr)
    {
        int max = arr[0];
        for(int i = 1; i < arr.Length; i++)
        {
            if (arr[i] > max)
            {
                max = arr[i];
            }
        }
        return max;
    } 
}