﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        //kết quả hàm sau khi thực thi là mảng số nguyên mới đã sắp xếp theo thứ tự giảm dần
        for (int i = 0; i < x.Length - 1; i++)
            for (int j = 0; j < x.Length - i - 1; j++)
                if (x[j] > x[j + 1])
                {
                    
                    int temp = x[j];
                    x[j] = x[j + 1];
                    x[j + 1] = temp;
                }
        return x;
    }

}